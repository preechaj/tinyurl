const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const { MongoClient } = require("mongodb");
const nanoid = require("nanoid");

const databaseUrl = process.env.DATABASE || "mongodb://localhost:27017";
const serverOrigin = process.env.SERVER_ORIGIN || "http://localhost:3000";

MongoClient.connect(databaseUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(client => {
    app.locals.db = client.db("tinyurl");
  })
  .catch(() => {
    console.error("Failed to connect to the database");
    process.exit(-1);
  });

const shortenURL = (db, url) => {
  const urls = db.collection("urls");
  return urls.findOneAndUpdate(
    { original_url: url },
    {
      $setOnInsert: {
        original_url: url,
        short_id: nanoid(7),
        created_at: new Date()
      }
    },
    {
      returnOriginal: false,
      upsert: true
    }
  );
};

const checkAlias = (db, url, alias) => {
  const urls = db.collection("urls");
  return urls.findOneAndUpdate(
    { short_id: alias },
    {
      $setOnInsert: {
        original_url: url,
        short_id: alias,
        created_at: new Date()
      }
    },
    {
      returnOriginal: true,
      upsert: true
    }
  );
};

const checkIfshort_idExists = (db, code) => {
  return db.collection("urls").findOne({ short_id: code });
};

const app = express();

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.json());

/**
 * redirect to original url
 */
app.get("/:short_id", (req, res, next) => {
  const short_id = req.params.short_id;

  const { db } = req.app.locals;
  checkIfshort_idExists(db, short_id)
    .then(doc => {
      if (doc === null) {
        res.redirect("/");
      } else {
        res.redirect(doc.original_url);
      }
    })
    .catch(next);
});

/**
 * Shorten url operation
 */
app.post("/new", (req, res, next) => {
  console.log(req.body);

  let originalUrl;
  try {
    originalUrl = new URL(req.body.url);
  } catch (err) {
    return res.status(400).send({ error: "invalid URL" });
  }

  console.log("originalUrl.origin", originalUrl.origin);

  // Avoid infinite redirect
  if (originalUrl.origin === serverOrigin) {
    throw new Error("URL domain banned");
  }

  const { db } = req.app.locals;

  shortenURL(db, originalUrl.href)
    .then(result => {
      const doc = result.value;
      const alias = req.body.alias;
      const shortenResult = {};

      shortenResult.original_url = doc.original_url;
      shortenResult.short_id = doc.short_id;
      shortenResult.alias = alias;

      if (alias) {
        // 'new' is reserved for shorten operation
        if(alias === 'new') {
          shortenResult.alias_available = false;
          res.json(shortenResult);
        } else {
          checkAlias(db, originalUrl.href, alias).then(aliasResult => {
            if (aliasResult.value === null) {
              shortenResult.alias_available = true;
            } else {
              shortenResult.alias_available = false;
            }
            res.json(shortenResult);
          });
        }
      } else {
        res.json(shortenResult);
      }
    })
    .catch(next);
});

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send({ error: err.message });
});

app.set("port", process.env.PORT || 3000);
const server = app.listen(app.get("port"), () => {
  console.log(`Start Express on port ${server.address().port}`);
});
