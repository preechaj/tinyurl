# tinyurl

Simple URL Shortener app (tinyurl)

## Prequisition for localhost demo
* Mongodb running on localhost with default port 27017

## A list of any security issues in your solution, and how you would fix them.
* I forgot about Web Login requirement.
* Not fully validate all input, no Custom Alias validation.

## A list of any scalability issues in your solution, and with how you would fix them.
* With 7 long of these characters (A-Za-z0-9_-), this system can handle 64^7 = 4,398,046,511,104 URLs, if there are 1,000,000,000 URLs shorten per day, we can run service for ~12 years.
* For nanoid(7), With 100 IDs per hour, ~124 days needed, in order to have a 1% probability of at least one collision. (https://zelark.github.io/nano-id-cc/), should properly handle re-generate ID to avoid duplicated ID error return.
* May require background process to clean up URLs without access for long time, but this also require last visit timestamp.
* With high-read/low-write rate, caching URLs that are frequently accessed will help speed up the system.