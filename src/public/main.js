new Vue({
  el: "#app",
  data: {
    processing: false,
    url: "",
    alias: undefined,
    result: undefined
  },
  methods: {
    shorten() {
      if(this.alias && !/^[a-z0-9]+$/i.test(this.alias)) {
        this.result = {
          error: "Custom alias only allow alphanumeric."
        }
        return;
      }

      this.processing = true;
      this.result = undefined;

      fetch("/new", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          url: this.url,
          alias: this.alias
        })
      })
        .then(response => {
          if (!response.ok) {
            this.result = undefined;
          }
          return response.json();
        })
        .then(data => {
          this.result = data;
          this.result.origin = location.origin;
        })
        .catch(err => {
          console.error("Error!", err);
          this.result = undefined;
        })
        .finally(() => {
          this.processing = false;
        });
    }
  }
});
